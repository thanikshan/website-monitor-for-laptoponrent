# Selenium Imports
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
import smtplib
import getpass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
# HTML imports
import requests
import os,time
from datetime import datetime  
from datetime import timedelta 


class LaptopOnRentCheck:

    def __init__(self):
        self.CHROMEDRIVERPATH = os.path.join(os.getcwd(), "chromedriver.exe")
        self.options = Options()
        self.headless = True
        self.appUrl = "https://laptoponrent.herokuapp.com/"
        self.subject="";
        self.currentTime=0;

    def checkTime(self):
        self.named_tuple = time.localtime();
        self.currentTime = time.strftime("%m-%d-%Y, %H:%M:%S", self.named_tuple) 

    @staticmethod
    def send_mail(subject):
        # Send email to given list of addresses
        server="smtp.gmail.com"
        port=587;
        use_tls=True;
        subject = subject + """ </table></body></html>"""
        message = MIMEText(subject, 'html')
        send_from="pvhome303@gmail.com"
        send_to ="tselva.16@gmail.com,pvhome303@gmail.com,sasisrikrishna@gmail.com,hariarun.19@gmail.com"
        message['From'] = send_from
        message['To'] = ", ".join(send_to.split(','))
        message['Date'] = formatdate(localtime=True)
        message['Subject'] = "Laptop On Rent - Status Check"
        smtp = smtplib.SMTP(server, port)
        if use_tls:
            smtp.starttls()
        smtp.login("pvhome303@gmail.com", "password")
        smtp.sendmail(send_from, send_to.split(','), message.as_string())
        smtp.quit()
        

    def setup_selenium(self):
        if self.headless:
            self.options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=self.options, executable_path=self.CHROMEDRIVERPATH)

    def checkHomepage(self):
        try:
            self.checkTime();
            self.driver.get(self.appUrl);
            time.sleep(5)
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-landing-page/div/div[4]/div/div[4]/div[1]/img').click()
            return a;
        except Exception as e:
            print(e);
            return 0;

    def checkSearch(self):
        try:
            self.checkTime();
            self.driver.get(self.appUrl);
            time.sleep(5)
            username_input = self.driver.find_element_by_xpath('//*[@id="searchBar"]')
            username_input.send_keys('Apple')
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/div/nav/div[2]/form/mat-form-field/div/div[1]/div[4]/button').click()
            time.sleep(5);
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-search-result/div/div/div[1]/div[1]/img');
            return a;
        except Exception as e:
            print(e);
            return 0;

    def checkProduct(self):
        try:            
            self.checkTime();
            self.driver.get(self.appUrl);
            time.sleep(5)
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-landing-page/div/div[3]/div/div[3]/div[1]/img').click()
            time.sleep(5);
            username_input = self.driver.find_element_by_xpath('//*[@id="enddate"]')
            prodDate= datetime.now() + timedelta(days=1);
            prodDate= prodDate.strftime("%m/%d/%Y");
            username_input.send_keys(str(prodDate))
            self.driver.find_element_by_xpath('//*[@id="quantity"]').click()
            self.driver.find_element_by_xpath('//*[@id="mat-option-6"]/span').click();
            self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-product-page/div/div[1]/div[3]/form/div[5]/button').click();
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-cart/div/div/div[1]/div/div[1]/div[1]/div/img')
            return a;
        except Exception as e:
            print(e);
            return 0;

    def checkCart(self):
        try:            
            self.checkTime();
            self.driver.get(self.appUrl);
            time.sleep(5)
            self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-landing-page/div/div[3]/div/div[3]/div[1]/img').click()
            time.sleep(5);
            username_input = self.driver.find_element_by_xpath('//*[@id="enddate"]')
            prodDate= datetime.now() + timedelta(days=1);
            prodDate= prodDate.strftime("%m/%d/%Y");
            username_input.send_keys(str(prodDate))
            self.driver.find_element_by_xpath('//*[@id="quantity"]').click()
            self.driver.find_element_by_xpath('//*[@id="mat-option-6"]/span').click();
            self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-product-page/div/div[1]/div[3]/form/div[5]/button').click();
            time.sleep(3)
            self.driver.find_element_by_xpath('//*[@id="carouselExampleControls"]/div[2]/div/button').click();
            a=self.driver.find_element_by_xpath('/html/body/app-root/app-user-navigation-bar/app-choose-address/div[1]/div[2]/div/button')
            return a;
        except Exception as e:
            print(e);
            return 0;

    def checkLogin(self):
        try:            
            self.checkTime();
            self.driver.get(self.appUrl);
            time.sleep(5)
            self.driver.find_element_by_xpath('//*[@id="profileDropDown"]/i').click();
            self.driver.find_element_by_xpath('//*[@id="menu"]/ul/li[3]/div/a').click();
            username_input = self.driver.find_element_by_xpath('//*[@id="username"]')
            username_input.send_keys('tselva.16@gmail.com')
            username_input = self.driver.find_element_by_xpath('//*[@id="password"]')
            username_input.send_keys('test123')
            self.driver.find_element_by_xpath('//*[@id="btnLogin"]').click();
            time.sleep(3);
            self.driver.find_element_by_xpath('//*[@id="profileDropDown"]/i').click();
            a=self.driver.find_element_by_xpath('//*[@id="menu"]/ul/li[3]/div/a[3]');
            return a;
        except Exception as e:
            print(e);
            return 0;



        

if __name__ == "__main__":

    i=0;


    while i == 0:
            
        check=0;

        lap=LaptopOnRentCheck();
        lap.setup_selenium();
        lap.subject+= """<!DOCTYPE html>
        <html>
        <head>
        <style>
        table, th, td {
          border: 1px dotted black;
        }
        </style>
        </head>
        <body>

        <h2>Laptop On Rent - Checklist </h2>

        <table>
          <tr>
            <th>Functionality</th>
            <th>Status</th>
            <th>Checked At</th>
          </tr>
        """
        #checkpages:
        a=lap.checkHomepage();
        print(a);
        
        if a != 0:
            lap.subject += """<tr> <td>Homepage</td> <td style="background-color:#00FF00">Green</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
            
        else:
            check+=1;
            lap.subject += """<tr> <td>Homepage</td> <td style="background-color:#FF0000">Red</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
            
        a=0;

        a=lap.checkSearch();
        if a != 0:
            lap.subject += """<tr> <td>Search</td> <td style="background-color:#00FF00">Green</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
        else:
            check+=1;
            lap.subject += """<tr> <td>Search</td> <td style="background-color:#FF0000">Red</td><td>{code}</td> </tr>""".format(code=lap.currentTime)

        a=lap.checkProduct();
        if a != 0:
            lap.subject += """<tr> <td>Product</td> <td style="background-color:#00FF00">Green</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
        else:
            check+=1;
            lap.subject += """<tr> <td>Product</td> <td style="background-color:#FF0000">Red</td><td>{code}</td> </tr>""".format(code=lap.currentTime)

        a=lap.checkLogin();
        if a != 0:
            lap.subject += """<tr> <td>Login</td> <td style="background-color:#00FF00">Green</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
        else:
            check+=1;
            lap.subject += """<tr> <td>Login</td> <td style="background-color:#FF0000">Red</td><td>{code}</td> </tr>""".format(code=lap.currentTime)

        a=lap.checkCart();
        if a != 0:
            lap.subject += """<tr> <td>Cart</td> <td style="background-color:#00FF00">Green</td><td>{code}</td> </tr>""".format(code=lap.currentTime)
        else:
            check+=1;
            lap.subject += """<tr> <td>Cart</td> <td style="background-color:#FF0000">Red</td><td>{code}</td> </tr>""".format(code=lap.currentTime)

        

        if check != 0:
            lap.send_mail(lap.subject);

        time.sleep(3600)
        print("done");

    
